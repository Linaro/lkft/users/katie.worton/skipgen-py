#!/usr/bin/env python3

import argparse
import os
import sys
import yaml
from pathlib import Path

"""
Skipgen generates a skiplist from a skipfile yaml.
"""

version = "TEST VERSION"


def standardise_format(skipfile):
    """
    Since some entries in the YAML can be either a single string or a list of
    strings, first convert all single strings to a list of length 1.
    Some branches can also be written in a float-like form, so cast to string
    to prevent ambiguity.
    """
    standardised_skiplist = dict()
    standardised_skiplist["skiplist"] = []

    for skiptest in skipfile["skiplist"]:
        for item in "boards", "environments", "branches", "tests":
            if not isinstance(skiptest[item], list):
                skiptest[item] = [skiptest[item]]
            skiptest[item] = [str(x) for x in skiptest[item]]
        standardised_skiplist["skiplist"].append(skiptest)

    return standardised_skiplist


def parse_skipfile(skipfile):
    """
    Try to load the skipfile as YAML.
    If the load is successful, return a standardised version of the skipfile dict
    """
    try:
        yaml.safe_load(skipfile)
    except yaml.YAMLError as exc:
        raise exc

    yaml_dict_skipfile = standardise_format(yaml.safe_load(skipfile))
    return yaml_dict_skipfile


def string_in_search_list(search_string, search_list):
    """
    Checks if:
        - the search string is "all",
        - the search list contains "all" or
        - the search list contains the search string
    """
    if search_string == "all":
        return True
    else:
        for item in search_list:
            if item == search_string or item == "all":
                return True
        return False


def get_skipfile_contents(board, branch, environment, skips):
    """
    Loop through the cases in the skipfile and return a skiplist based on the
    search parameters
    """
    skiplist = []
    for skip in skips["skiplist"]:
        if (
            string_in_search_list(board, skip["boards"])
            and string_in_search_list(branch, skip["branches"])
            and string_in_search_list(environment, skip["environments"])
        ):
            for test in skip["tests"]:
                if test not in skiplist:
                    skiplist.append(test)
    result = sorted(skiplist)

    return result


def run(args):
    # Read skipfile
    skipfile = Path(args.skipfile).read_text()
    # Parse skipfile
    skips = parse_skipfile(skipfile)

    print("\n".join(get_skipfile_contents(args.board, args.branch, args.environment, skips)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--board",
        default="all",
        help="Board name. If not specified, skips for all boards will be returned.",
        required=False,
    )
    parser.add_argument(
        "--branch",
        default="all",
        help="Branch name. If not specified, skips for all branches will be returned.",
        required=False,
        type=str,
    )
    parser.add_argument(
        "--environment",
        default="all",
        help="Environment name. If not specified, skips for all environments will be returned.",
        required=False,
    )
    parser.add_argument(
        "--version",
        help="Print skipgen version and exit.",
        required=False,
        action="store_true",
    )
    parser.add_argument(
        "--skipfile",
        default=None,
        help="The skipfile to parse.",
        required=False,
    )
    args = parser.parse_args()

    if args.version:
        print(version)
        sys.exit(0)
    elif not args.skipfile:
        parser.print_usage()
        sys.exit(1)
    else:
        if not os.path.exists(args.skipfile):
            parser.print_help()
            raise OSError("Skipfile does not exist")

    if args:
        run(args)
    else:
        exit(1)
