#!/usr/bin/env python3

import argparse
import os
import pathlib
from schema import Or, Schema, SchemaError
import yaml


config_schema = Schema(
    {
        "skiplist": [
            {
                "reason": str,
                "url": str,
                "environments": Or([str], str),
                "boards": Or([str], str),
                "branches": Or(
                    [str], str, [object]
                ),  # TODO - branches like 4.4 are not parsed as a string
                "tests": Or([str], str),
            }
        ]
    }
)


def validate_skipfile(skipfile):
    """
    Validate skipfile
    """
    skips = yaml.safe_load(skipfile)

    try:
        config_schema.validate(skips)
        print("Skipfile is valid.")
    except SchemaError:
        raise SchemaError("The skipfile does not follow the schema.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--skipfile",
        default=None,
        help="The skipfile to parse.",
        required=True,
    )
    args = parser.parse_args()

    if not os.path.exists(args.skipfile):
        raise OSError("Skipfile does not exist.")

    # Read skipfile
    skipfile = pathlib.Path(args.skipfile).read_text()

    validate_skipfile(skipfile)
