#!/usr/bin/env python3

import argparse
import os
import pathlib
import sys
from ruamel.yaml import YAML
import validate_skipfile_schema

"""
(WIP) Tools to update the skipfile.
Example use - removing an unsupported branch across a skipfile.
"""


def add_skiplist_entry(skipfile, args):
    """
    Add a new entry to the skiplist
    """
    new_entry = {
        "reason": args.reason,
        "url": args.url,
        "environments": args.environments,
        "boards": args.boards,
        "branches": args.branches,
        "tests": args.tests,
    }
    skipfile["skiplist"].append(new_entry)


def delete_skiplist_entry(skipfile, args):
    """
    Delete an entry from the skiplist (for example, when a bug has been fixed)
    """
    skipfile["skiplist"] = [
        test for test in skipfile["skiplist"] if args.url != test["url"]
    ]


def delete_skiplist_entry_component(test, args):
    """
    Remove an item from a skiplist entry's components (for example, removing
    a test from the `tests` component)
    """
    for item in args.update_arguments:
        test[args.change].remove(item)

    assert test[args.change], (
        "ERROR: removed all entries to component:",
        args.change,
        test["url"],
    )


def replace_skiplist_entry_component(test, args):
    """
    Replace the entry of a skiplist component (e.g. boards, environment,
    tests, branches) with a new value
    """
    test[args.change] = list(set(args.update_arguments))


def add_skiplist_entry_component(test, args):
    """
    Add an entry to a skiplist component (e.g. boards, environment,
    tests, branches)
    """
    if not isinstance(test[args.change], list):
        test[args.change] = [test[args.change]]
    test[args.change] += args.update_arguments


def convert_to_list_of_string(test):
    if not isinstance(test[args.change], list):
        test[args.change] = [test[args.change]]
    test[args.change] = [str(item) for item in test[args.change]]


def update_entry(skipfile, args):
    """
    Loop through the skiplist and perform the appropriate update
    """
    for test in skipfile["skiplist"]:
        if args.url == test["url"] or args.update_type == "remove_all_instances":
            # When we perform an update, this is easier if we work with just lists
            # rather than a mix of lists and strings.
            # So, convert to list of strings before operating.
            convert_to_list_of_string(test)
            if (
                args.update_type == "remove"
                or args.update_type == "remove_all_instances"
            ) and (set(args.update_arguments) & set(test[args.change])):
                delete_skiplist_entry_component(test, args)
            elif args.update_type == "replace":
                replace_skiplist_entry_component(test, args)
            elif args.update_type == "add":
                add_skiplist_entry_component(test, args)


def run(args):
    skipfile_txt = pathlib.Path(args.skipfile).read_text()
    yaml = YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)
    skipfile = yaml.load(skipfile_txt)

    if args.url or (args.update_type == "remove_all_instances"):
        if args.update_type == "delete_entry":
            delete_skiplist_entry(skipfile, args)
        elif args.update_type == "new_entry":
            add_skiplist_entry(skipfile, args)
        else:
            update_entry(skipfile, args)

    with open(args.skipfile_output, "w") as test_yaml:
        yaml.dump(skipfile, test_yaml)

    skipfile = pathlib.Path(args.skipfile_output).read_text()
    validate_skipfile_schema.validate_skipfile(skipfile_txt)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help="Choose a command")

    parser.add_argument(
        "--url",
        default=False,
        help="URL for the bug that means we require a skip.",
        required=False,
    )
    parser.add_argument(
        "--change",
        default=False,
        help="Which element to change.",
        required=False,
        choices=["tests", "branches", "environments", "boards", "reason", "url"],
    )
    parser.add_argument(
        "--update-type",
        default=False,
        help="The type of update to perform.",
        required=True,
        choices=[
            "add",
            "remove",
            "replace",
            "new_entry",
            "delete_entry",
            "remove_all_instances",
        ],
    )
    parser.add_argument(
        "--update-arguments",
        default=False,
        help="Arguments the update will use.",
        nargs="+",
        required=False,
    )
    parser.add_argument(
        "--reason",
        default=False,
        help="The reason for the skip.",
    )
    parser.add_argument("--boards", default=False, help="The skip's boards", nargs="+")
    parser.add_argument(
        "--environments", default=False, help="The skip's environments", nargs="+"
    )
    parser.add_argument(
        "--branches", default=False, help="The skip's branches", nargs="+"
    )
    parser.add_argument("--tests", default=False, help="The skip's tests", nargs="+")
    parser.add_argument(
        "--skipfile",
        default=None,
        help="The input skipfile.",
        required=False,
    )
    parser.add_argument(
        "--skipfile_output",
        default="new_skipfile.yaml",
        help="The output (updated) skipfile.",
        required=False,
    )
    args = parser.parse_args()

    if args.skipfile and not os.path.exists(args.skipfile):
        parser.print_help()
        raise OSError("Skipfile does not exist")
    elif args.update_type == "new_entry":
        assert (
            args.reason
            and args.url
            and args.boards
            and args.tests
            and args.branches
            and args.environments
        ), "To add a new entry these fields are required: "\
        "reason, url, environments, boards, branches, tests"
        run(args)
    elif args.skipfile:
        assert (
            args.update_arguments
            and args.update_type
            and args.change
        ), "The following arguments are required to perform an update: "\
            "update_arguments, update_type and change"
        assert (
            args.update_type == "remove_all_instances" or args.url
        ), "A URL is required unless performing remove_all_instances."
        run(args)
    else:
        parser.print_help()
