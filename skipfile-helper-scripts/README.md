# Overview

This directory contains scripts to update and validate skipfiles.

These scripts are currently a work-in-progress.

## Installing Requirements

    pip install -r requirements.txt


## Validate Skipfile Schema (WIP)

This script can be used to check all the skiplist entries contain all the
required fields.

### Usage

    ./validate_skipfile_schema.py [-h] [--skipfile SKIPFILE]

### Example Usage

    ./validate_skipfile_schema.py --skipfile ../examples/skipfile.yaml


## Update Skipfile (WIP)

This script can be used to update the skiplist in a skipfile.

NOTE: This currently has few input validation checks.

### Usage

    ./skipfile_updater.py [-h] [--url URL] [--change {tests,branches,environments,boards}] [--update-type {add,remove,replace,new_entry,delete_entry,remove_all_instances}] [--update-arguments UPDATE_ARGUMENTS [UPDATE_ARGUMENTS ...]] [--reason REASON]
                           [--boards BOARDS [BOARDS ...]] [--environments ENVIRONMENTS [ENVIRONMENTS ...]] [--branches BRANCHES [BRANCHES ...]] [--tests TESTS [TESTS ...]] [--skipfile SKIPFILE]


### Example Usage
    
    ./skipfile_updater.py --skipfile ../examples/skipfile.yaml --update-type remove --url https://bugs.linaro.org/show_bug.cgi?id=3503 --change branches --update-arguments 4.4 4.9 --skipfile_output=new_skipfile.yaml

    ./skipfile_updater.py --skipfile ../examples/skipfile.yaml --update-type remove_all_instances --change branches --update-arguments 4.4 4.9 --skipfile_output=new_skipfile.yaml

    ./skipfile_updater.py --skipfile ../examples/skipfile.yaml --update-type remove_all_instances --change tests --update-arguments test_verifier --skipfile_output=new_skipfile.yaml

    ./skipfile_updater.py --skipfile ../examples/skipfile.yaml --update-type new_entry --reason "Test reason" --url https://bugs.linaro.org --environments all --boards all --branches all --tests test1 test2


