# Overview

This is a Python port of the Linaro Skiplist Generator (skipgen). The code in 
this port is a derivative work based on the original Golang implemetation:
https://github.com/Linaro/skipgen. Some components have been reused or adapted
from the original repository.

Skiplist Generator (skipgen) is a program that will generate a skiplist given a
yaml file and optionally a board name, branch name, and environment name.

## Usage

    ./skipgen.py [-h] [--board BOARD] [--branch BRANCH] [--environment ENVIRONMENT] [--version] [--skipfile SKIPFILE]

## Example Usage

Show all skips available:

    $ ./skipgen.py --skipfile examples/skipfile.yaml
    breakpoint_test_arm64
    ftracetest
    fw_filesystem.sh
    pstore_tests
    run.sh
    run_fuse_test.sh
    run_vmtests
    seccomp_bpf
    ...

Show skips that apply to the x15 board in the production environment and branch 4.4:

    $ ./skipgen.py --board=x15 --environment=staging --branch=4.4 --skipfile=examples/skipfile.yaml
    run_vmtests
    seccomp_bpf

## Skipfile Format

See [examples/skipfile.yaml](examples/skipfile.yaml).

## Installing Requirements

    pip install -r requirements.txt

## Testing

Skipgen includes Python unit tests that can be run using `./skipgen_test.py`.

    ./skipgen_test.py -v
