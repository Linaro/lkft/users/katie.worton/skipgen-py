#!/usr/bin/env python3

import unittest
import skipgen


class TestSkipgen(unittest.TestCase):
    def test_string_in_search_list_positive(self):
        slice = ["one", "two", "three", "four", "five"]

        result = skipgen.string_in_search_list("one", slice)
        self.assertTrue(result)

    def test_string_in_search_list_negative(self):
        slice = ["one", "two", "three", "four", "five"]

        result = skipgen.string_in_search_list("zero", slice)
        self.assertFalse(result)

    def test_skip_all(self):
        skipAll = """
skiplist:
  - reason: kernel tests baselining
    url: https://projects.linaro.org/projects/CTT/queues/issue/CTT-585
    environments: all # Test this form
    boards:
      - all # And test this form
    branches:
      - all
    tests:
      - test_maps
      - test_lru_map
      - test_lpm_map
      - test_progs
  - reason: "LKFT: linux-next: vm compaction_test : ERROR: Less that 1/730 of memory
             is available"
    url: https://bugs.linaro.org/show_bug.cgi?id=3145
    environments:
      - production
      - staging
    boards:
      - x15
      - juno
      - hikey
    branches:
      - "4.4"
      - 4.9
      - mainline
    tests:
      - run_vmtests
  - reason: duplicate skip for specific board combination
    url: https://bugs.linaro.org/show_bug.cgi?id=3145
    environments:
      - production
    boards:
      - x86
    branches:
      - 4.14
    tests:
      - run_vmtests
        """
        skips = skipgen.parse_skipfile(skipAll)
        self.assertEqual(
            skips["skiplist"][0]["reason"],
            "kernel tests baselining",
            "Incorrect Skipfile Contents",
        )

        result = skipgen.get_skipfile_contents("x15", "4.4", "production", skips)
        expect_result = """run_vmtests
test_lpm_map
test_lru_map
test_maps
test_progs"""

        self.assertMultiLineEqual(result, expect_result, "Incorrect Skipfile Contents")

        result = skipgen.get_skipfile_contents("all", "all", "all", skips)
        expect_result = """run_vmtests
test_lpm_map
test_lru_map
test_maps
test_progs"""
        self.assertMultiLineEqual(result, expect_result, "Incorrect Skipfile Contents")

    def test_skip_minimum(self):
        skipAll = """skiplist:
  - reason: Some test reason
    url: https://bugs.linaro.org/show_bug.cgi?id=3145
    environments: production # Test UnmarshallYAML
    boards: x15 # Test UnmarshallYAML
    branches: "4.4" # Test UnmarshallYAML
    tests: run_vmtests # Test UnmarshallYAML"""
        skips = skipgen.parse_skipfile(skipAll)
        self.assertEqual(
            skips["skiplist"][0]["reason"],
            "Some test reason",
            "Parsing error, skiplist is wrong",
        )

        result = skipgen.get_skipfile_contents("x15", "4.4", "production", skips)
        expected = "run_vmtests"
        self.assertEqual(result, expected, "Incorrect Skipfile Contents")

        result = skipgen.get_skipfile_contents("x15", "4.9", "production", skips)
        expected = ""
        self.assertEqual(result, expected, "Incorrect Skipfile Contents")


if __name__ == "__main__":
    unittest.main()
